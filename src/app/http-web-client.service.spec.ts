import { TestBed } from '@angular/core/testing';

import { HttpWebClientService } from './http-web-client.service';

describe('HttpWebClientService', () => {
  let service: HttpWebClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpWebClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
