import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { User } from './ReqTypes';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public router: Router) { }
  canActivate(): boolean {
    let user:User = JSON.parse(localStorage.getItem('user'))
    if (!user) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}
