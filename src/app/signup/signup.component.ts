import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {User} from '../ReqTypes';
import {HttpWebClientService} from '../http-web-client.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public emailInput:string;
  constructor(private _router:Router,private httpclient:HttpWebClientService) { }

  ngOnInit(): void {
  }

  public validateEmail(email):boolean {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  public validateData():boolean{
    if(!this.emailInput || this.validateEmail(this.emailInput) == false)
    {
      alert("please enter email address")
      return false;
    }else
    {
      return true;
    }
  }

  public onSubmitClick():void {
    if(this.validateData() == true)
    {
       //hit api here
       this.httpclient.signOn(this.emailInput).subscribe({
        next: data => {
            console.log(data);
            if(Object.getOwnPropertyNames(data['data']).length === 0)
            {
              alert(data['err'])
            }
            else
            {
              //store data in localstorage for retreiving later
              let user:User = {
                  'id':data['data']['id'],
                  'email':this.emailInput
              }
              localStorage.setItem('user',JSON.stringify(user));
              this._router.navigateByUrl('/startQuiz');
            }
               
        },
        error: error => {
              console.error('There was an error!', error);
        }
       });        
        
    }
  }

}
