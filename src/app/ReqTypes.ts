export interface User {
    id:number,
    email:string
}

export interface QuizApiEmbeddedAnswer{
    
        'questionId':number,
        'optionId':number
    
}

export interface QuizApiOPtions {
    'id':number,
    'questionId':number,
    'option':string,
    
}

export interface QuizApiObject {
    'id':number,
    'question':string,
    'options':Array<QuizApiOPtions>,
    'answer':QuizApiEmbeddedAnswer
}