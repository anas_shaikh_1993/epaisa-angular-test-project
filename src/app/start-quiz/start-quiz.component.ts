import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start-quiz',
  templateUrl: './start-quiz.component.html',
  styleUrls: ['./start-quiz.component.css']
})
export class StartQuizComponent implements OnInit {

  constructor(private _router:Router) { }

  ngOnInit(): void {
  }

  public onStartQuizBtnClick():void{
      this._router.navigateByUrl('quiz')
  }

}
