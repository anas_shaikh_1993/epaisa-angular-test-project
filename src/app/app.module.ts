import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { StartQuizComponent } from './start-quiz/start-quiz.component';
import { QuizComponent } from './quiz/quiz.component';
import {AuthGuardService} from './auth-guard.service';
import { AnswerSectionComponent } from './answer-section/answer-section.component';
import {AnswerServiceService} from './answer-service.service'
import {HttpWebClientService} from './http-web-client.service';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    StartQuizComponent,
    QuizComponent,
    AnswerSectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthGuardService,AnswerServiceService,HttpWebClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
