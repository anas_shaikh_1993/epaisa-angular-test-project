import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { StartQuizComponent } from './start-quiz/start-quiz.component';
import { QuizComponent } from './quiz/quiz.component';
import {AuthGuardService as AuthGuard} from './auth-guard.service';
import { AnswerSectionComponent } from './answer-section/answer-section.component';

const routes: Routes = [
  { path: '', component: SignupComponent },
  { path: 'startQuiz',component: StartQuizComponent,canActivate: [AuthGuard]},
  { path: 'quiz',component: QuizComponent,canActivate: [AuthGuard]},
  { path: 'answerSection',component: AnswerSectionComponent,canActivate: [AuthGuard]},
  {
    "path": "**",
    "redirectTo": "error_404",
    "pathMatch": "full"
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
