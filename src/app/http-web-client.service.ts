import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpWebClientService {
  private localUrl:string = "http://localhost:3000/"
  constructor(private http: HttpClient) { }

  public signOn(emailId:string) : Observable<any>{
     return this.http.post<any>(this.localUrl + "user",{'emailId':emailId});
  }

  public getQuizData():Observable<any>{
    return this.http.get<any>(this.localUrl + "questions");
  }

  public submitAnswer(userId:number,questionId:number,optionId:number):Observable<any>{
    return this.http.post<any>(this.localUrl + "submitAnswer",{'userId':userId,'questionId':questionId,'optionId':optionId});
  }
  //final commit
  public getTotalScore(userId:number):Observable<any>{
    return this.http.get<any>(this.localUrl + "getTotalScore/" +userId);
  }
}
