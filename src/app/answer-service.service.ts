import { Injectable } from '@angular/core';
const writeAnserScore:number = 100;
@Injectable({
  providedIn: 'root'
})



export class AnswerServiceService {
  private score:number;

  constructor() {
    this.score = 0;
  }

  public setScore():void{
    this.score = this.score + writeAnserScore;
  }

  public getScore():number{
    return this.score;
  }

  public clear():void {
    this.score = 0;
  }
}
