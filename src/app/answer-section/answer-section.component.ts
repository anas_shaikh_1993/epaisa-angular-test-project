import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AnswerServiceService} from '../answer-service.service';
import {HttpWebClientService} from '../http-web-client.service';
import { User } from '../ReqTypes';

@Component({
  selector: 'app-answer-section',
  templateUrl: './answer-section.component.html',
  styleUrls: ['./answer-section.component.css']
})
export class AnswerSectionComponent implements OnInit {
  public totalScore:number = 0;
  constructor(private _router:Router,private answerService:AnswerServiceService,private httpclient:HttpWebClientService) { }

  ngOnInit(): void {
    let user:User = JSON.parse(localStorage.getItem('user'));
    this.httpclient.getTotalScore(user.id).subscribe({
      next:data => {
        console.log(data);
        if(data['code'] == 200)
        {
           this.totalScore = data['data']['totalScore'];
        }
        else
        {
          this.totalScore = 0;
        }
      },
      error:err => {
        console.error(err)
      }
    })
    
  }

  public onBtnClickEvent():void{
    this.answerService.clear();
    localStorage.removeItem('user')
    this._router.navigateByUrl('')
  }

}
