import { Component, OnInit,AfterViewInit,OnDestroy } from '@angular/core';
import {QuizApiObject,QuizApiOPtions,QuizApiEmbeddedAnswer, User} from '../ReqTypes';
import {ServerData} from './data-simulation';
import { interval, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import {AnswerServiceService} from '../answer-service.service';
import {HttpWebClientService} from '../http-web-client.service';
@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit,AfterViewInit,OnDestroy {
  subscription: Subscription;
  timersubscription: Subscription;
  public serverData:Array<QuizApiObject> = [];
  public counter:number = 0;
  public maxQuestion:number = 4;
  public question:String = "";
  public options:Array<QuizApiOPtions> = [] ;
  public toDisable:boolean = false;
  public displayRightAnswer:boolean = false;
  public displayWrongAnswer:boolean = false;
  public remainingTime:number = 15;
  
  
  constructor(private _router:Router,private answerService:AnswerServiceService,private httpclient:HttpWebClientService) { }

  ngOnInit(): void {
    this.httpclient.getQuizData().subscribe({
      next:data => {
        if(data['code'] == 200)
        {
          this.serverData = data['data'];
          this.question = this.serverData[this.counter].question;
          this.options = this.serverData[this.counter].options;
          const source = interval(15000);
          this.subscription = source.subscribe(val => {
            this.moveToNextQuestion()
          });
          const timerEvent = interval(1000);
          this.timersubscription = timerEvent.subscribe(val => {
            this.remainingTime = this.remainingTime  > 0 ? this.remainingTime - 1 : 0;
          }); 
        }else
        {
          console.error(data['err'])
        }
       
      },
      error:err => {
        console.error(err);
        this.serverData = [];
        this.question = "";
        this.options = [];    
      }
    });
    
  }

  

  ngAfterViewInit():void {

    // const source = interval(15000);
    // this.subscription = source.subscribe(val => {
    //   this.moveToNextQuestion()
    // });
    // const timerEvent = interval(1000);
    // this.timersubscription = timerEvent.subscribe(val => {
    //   this.remainingTime = this.remainingTime  > 0 ? this.remainingTime - 1 : 0;
    // }); 
  }

  public unsubscribe(){
    this.subscription && this.subscription.unsubscribe();
    this.timersubscription && this.timersubscription.unsubscribe();
  }

  public moveToNextQuestion(){
    this.remainingTime = 16; // adding 1 second as delay
    this.toDisable = false;
    this.displayRightAnswer = false;
    this.displayWrongAnswer = false;
    this.counter = this.counter  + 1;
    if(this.counter >= 5){
      this.unsubscribe();
      this.timersubscription && this.timersubscription.unsubscribe();
      this.moveToAnswerSection()
    }
    try{
      this.question = this.serverData[this.counter].question;
      this.options = this.serverData[this.counter].options;
    }catch(e){
      this.unsubscribe();
      this.moveToAnswerSection()
    }
    

  } 
  public moveToAnswerSection():void {
    console.log("i m answer section")
    this._router.navigateByUrl('/answerSection')
  }

  public OnMarkedAnswerEvent(item:QuizApiOPtions):void {
      this.toDisable = true;
      let user:User = JSON.parse(localStorage.getItem('user'));
      this.httpclient.submitAnswer(user.id,item.questionId,item.id).subscribe({
        next:data => {
          if(data['code'] == 200)
          {
             if(data['data']['incremented'] == true)
             {
              this.displayRightAnswer = true
             }
             else
             {
              this.displayWrongAnswer = true
             }
          }
          else
          {
            console.error("something went wrong");
          }
        },
        error:err => {console.log(err);}
      });
      // let answer:QuizApiEmbeddedAnswer = this.serverData[this.counter].answer;
      
      // if(item.questionId == answer.questionId){
      //   if(item.id == answer.optionId){
      //     this.answerService.setScore();
      //     this.displayRightAnswer = true
      //   }else{
      //     this.displayWrongAnswer = true
      //   }
      // }
      
      // console.log("--------------------------")
      // console.log(item)
      // console.log("--------------------------")
  }

  ngOnDestroy() {
    this.timersubscription && this.timersubscription.unsubscribe();
    this.subscription && this.subscription.unsubscribe();
  }

  

}
